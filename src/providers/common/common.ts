import {BleTag} from "../../types/ble-tag";

export class Common {

  public static searchId = '';

  public static calculateRssiForTag(tag: BleTag): BleTag {
    const bytes = new Uint8Array(tag.advertising);

    // determine real length
    let length = bytes.length;
    for (let i = bytes.length - 1; i >= 0; i--) {
      if (bytes[i] !== 0) {
        length = i + 1;
        break;
      }
    }

    // transform to hex
    tag.hex = '';
    for (let i = 0; i < length; i++) {
      let byte = bytes[i].toString(16).toUpperCase();
      if (byte.length === 1) {
        byte = '0' + byte;
      }
      tag.hex += byte;
    }

    tag.devEUI = tag.hex.substr(8, 16);
    tag.perc = this.rssiToPercentage(tag.rssi);
    tag.time = +new Date();

    return tag;
  }

  private static rssiToPercentage(rssi: number): number {
    return (-100 * Math.pow(10, rssi / -100) + 100) / 9 + 100;
  }

}
