import {Component, NgZone} from '@angular/core';
import {BLE} from "@ionic-native/ble";
import {Common} from "../../providers/common/common";
import {BleTag} from "../../types/ble-tag";
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'page-search',
  templateUrl: 'ble-search.html'
})
export class BleSearch {

  public searchedDevEUI: string = '';
  public devices: BleTag[] = [];

  private observable: Observable<any>;
  private subscription: Subscription;

  constructor(private zone: NgZone, private ble: BLE) {
    this.bleSignalFound.bind(this);
  }

  public ionViewDidLoad = (): void => {
    console.log('BleDevices component did load');
  };

  public ionViewWillEnter = (): void => {
    console.log('BleDevices component will enter');
    this.devices = [];
    this.observable = this.ble.startScanWithOptions([], {reportDuplicates: true});
  };

  public ionViewDidEnter = (): void => {
    console.log('BleDevices component did enter');
    this.searchedDevEUI = Common.searchId;
    this.subscription = this.observable.subscribe(this.bleSignalFound);
  };

  public ionViewWillLeave = (): void => {
    console.log('BleDevices component will leave');
    this.subscription.unsubscribe();
  };

  public ionViewDidLeave = (): void => {
    console.log('BleDevices component did leave');
    this.subscription = null;
    this.observable = null;
  };

  public ionViewDidUnload = (): void => {
    console.log('BleDevices component did unload');
  };

  public onInput = ($event): void => {
    this.devices = [];
    Common.searchId = this.searchedDevEUI;
  };

  private bleSignalFound = (tag): void => {
    let bleTag: BleTag = Common.calculateRssiForTag(tag);

    this.zone.run(() => {
      if (bleTag.devEUI.toLowerCase() === this.searchedDevEUI.toLowerCase())
        this.devices.unshift(bleTag);
    })

  };


}
