import {Component} from '@angular/core';

import {BleDevices} from '../ble-devices/ble-devices';
import {BleSearch} from '../ble-search/ble-search';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  public tab1Root = BleDevices;
  public tab2Root = BleSearch;

  constructor() {

  }
}
