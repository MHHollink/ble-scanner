import {Component, NgZone} from '@angular/core';
import {BLE} from "@ionic-native/ble";
import {BleTag} from "../../types/ble-tag";
import {Common} from "../../providers/common/common";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import {NavController} from "ionic-angular";

@Component({
  selector: 'page-list',
  templateUrl: 'ble-devices.html'
})
export class BleDevices {

  public devices: BleTag[] = [];

  private observable: Observable<any>;
  private subscription: Subscription;

  constructor(private zone: NgZone, private navCtrl: NavController, private ble: BLE) {
    this.bleSignalFound.bind(this);
  }

  public ionViewDidLoad = (): void => {
    console.log('BleDevices component did load');
  };

  public ionViewWillEnter = (): void => {
    console.log('BleDevices component will enter');
    this.observable = this.ble.startScanWithOptions([], {reportDuplicates: true});
  };

  public ionViewDidEnter = (): void => {
    console.log('BleDevices component did enter');
    this.subscription = this.observable.subscribe(this.bleSignalFound);
  };

  public ionViewWillLeave = (): void => {
    console.log('BleDevices component will leave');
    this.subscription.unsubscribe();
  };

  public ionViewDidLeave = (): void => {
    console.log('BleDevices component did leave');
    this.subscription = null;
    this.observable = null;
  };

  public ionViewDidUnload = (): void => {
    console.log('BleDevices component did unload');
  };

  public itemOnClick = (elm: BleTag): void => {
    Common.searchId = elm.devEUI;
    this.navCtrl.parent.select(1);
  };

  private bleSignalFound = (tag): void => {
    let bleTag: BleTag = Common.calculateRssiForTag(tag);

    let index = this.devices.map(tag => tag.devEUI).indexOf(bleTag.devEUI);

    this.zone.run(() => {
      if (index === -1) {
        this.devices.push(bleTag)
      } else {
        this.devices[index] = bleTag;
      }

      this.devices.sort((a, b) => ('' + a.devEUI).localeCompare(b.devEUI));
    });
  };

}
