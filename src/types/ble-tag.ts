export type BleTag = {
  advertising: any;
  hex?: string;
  devEUI?: string;
  rssi?: number;
  perc?: number;
  time: number;
}
