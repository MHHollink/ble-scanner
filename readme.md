# BLE Scanner

## Development

The application is made with the Ionic Framework for easy development & cross platform capabilities. The app uses the [Ionic Native BLE plugin ](https://ionicframework.com/docs/native/ble/) for its BLE scanning. This results in losing browser support. The plugin works on both Android and iOS.

## Building the app

https://ionicframework.com/docs/intro/deploying/
